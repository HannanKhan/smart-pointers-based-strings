#include "stringBuffer.h"
#include <iostream>

using namespace std;

void testShallowCopy()
{
	StringBuffer * a = new StringBuffer();

	cout << endl << "\t\tTesting Shallow Copy" << endl;
	a->reserve(2);
	cout << endl << endl << "Creating a stringBuffer a\nappending \'1\' in stringBuffer a";
	a->append('1');

	cout << endl << endl << "Copying stringBuffer a to stringBuffer b using copy constructor";
	StringBuffer * b = new StringBuffer(*a);

	cout << endl << endl << "Checking if both have same neighbouring list, Expected result : 1 or true \n\t\t result -> ";
	cout << (a->neighbours == a->neighbours) << endl;

	if (a->neighbours == a->neighbours)
		cout << "\t Test Passed!";
	else
		cout << "\t Test Failed!";
	cout << endl << "################################################################################" << endl;
}

void testCheckRefCountDecrease()
{
	StringBuffer * a = new StringBuffer();

	cout << endl << "\t\tTesting Ref Count" << endl;
	a->reserve(2);
	cout << endl << endl << "Creating a stringBuffer a\nappending \'1\' in stringBuffer a";
	a->append('1');

	cout << endl << endl << "Copying stringBuffer a to stringBuffer b using copy constructor";
	StringBuffer * b = new StringBuffer(*a);

	cout << endl << endl << "Checking list size, Expected result : 2 \n\t\t result-> ";
	cout << b->neighbours->size() << endl;

	cout << endl << endl << "Deleting a and checking list size, Expected result : 1 \n\t\t result -> ";
	delete a;
	a = nullptr;
	cout << b->neighbours->size() << endl;

	if (b->neighbours->size() == 1)
		cout << "\t Test Passed!";
	else
		cout << "\t Test Failed!";
	cout << endl << "################################################################################" << endl;
}
