#include "stringBuffer.h"
#include <iostream>

using namespace std;

void testShallowCopy() 
{
	StringBuffer * a = new StringBuffer();

	cout <<endl << "\t\tTesting Shallow Copy" <<endl;
	a->reserve(2);
	cout <<endl << endl << "Creating a stringBuffer a\nappending \'1\' in stringBuffer a";
	a->append('1');

	cout <<endl << endl << "Copying stringBuffer a to stringBuffer b using copy constructor";
	StringBuffer * b = new StringBuffer(*a);

	cout <<endl << endl << "Deleting stringBuffer b\n Trying to access first index of stringBuffer a, Expected result : Exception or garbage value \n\t\t result -> ";
	delete b;
	cout << a->charAt(0) <<endl;

	if (a->charAt(0) != '1')
		cout << "\t Test Passed!";
	else
		cout << "\t Test Failed!";
	cout <<endl << "################################################################################" <<endl;
}

void testCharStarToStringBuffer()
{
	char * a = new char[2];
	a[0] = '1';
	a[1] = '2';

	cout << endl << "\t\tTesting char* to StringBuffer convertion using constructor" << endl;
	cout << endl << endl << "Creating char* a with value \"12\"\n\n Creating StringBuffer b and passing a to b as parameters in constructor";
	

	cout << endl << endl << "Copying stringBuffer a to stringBuffer b using copy constructor";
	StringBuffer * b = new StringBuffer(a, 2);

	cout << endl << endl << "Checking charAt[1] \n Expected result : 2 \n\t\t result -> ";
	cout << b->charAt(1) <<endl;

	if (b->charAt(1) == '2')
		cout << "\t Test Passed!";
	else
		cout << "\t Test Failed!";

	delete b;
	cout <<endl << "################################################################################" <<endl;
}
